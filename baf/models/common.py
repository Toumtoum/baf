# -*- coding: utf-8 -*-
import json
import pprint


class Common(object):

    def __repr__(self):
        return pprint.pformat(self.to_dict())

    def to_dict(self):
        return dict(
            __type=self.__class__.__name__,
        )

    def to_json(self):
        return json.dumps(self.to_dict(), encoding='utf-8')
