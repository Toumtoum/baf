# -*- coding: utf-8 -*-
import logging

import sqlalchemy as sa
from sqlalchemy import orm

from .common import Common
from .meta import Base

log = logging.getLogger(__name__)


class Folder(Base, Common):
    __tablename__ = 'folder'

    id = sa.Column(sa.Integer, primary_key=True)
    "Identifier of the folder"
    path = sa.Column(sa.Text, index=True)
    "Absolute path of the Folder"

    @classmethod
    def from_path(cls, dbsession, path):
        return dbsession.query(cls).filter(
            cls.path == path
        ).first()
